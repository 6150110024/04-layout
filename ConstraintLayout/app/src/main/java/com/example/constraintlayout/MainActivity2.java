package com.example.constraintlayout;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Intent i = getIntent();
        String inputString = i.getStringExtra("yourName");
        String yourPhone = i.getStringExtra("yourID");
        TextView view = (TextView) findViewById(R.id.tvShow);
        view.setText("Hi, " + inputString + "Your age is " + yourPhone.toString());
    }
        public void cancel(View view){
            finishAffinity();

    }
}